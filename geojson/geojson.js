// !preview r2d3 data = jsonlite::read_json("geojson/test_geojson2.json"), d3_version = 4

var projection = d3.geoMercator();
var path = d3.geoPath().projection(projection);

projection.fitSize([width,height],data); // adjust the projection to the features

svg.attr("class", "tracts")
    .selectAll("path")
    .data(data.features)
    .enter()
        .append("path")
        .style("fill", "none")
        .style("stroke", "black")
        .attr("d", path);
    
// Asign class to geounit
// Tranisition to new path.
    
    
