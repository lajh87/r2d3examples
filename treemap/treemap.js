// !preview r2d3 data=jsonlite::read_json("inst/json/flare-2.json"), d3_version = 6, container = "div"
//
// r2d3: https://rstudio.github.io/r2d3

// Create an SVG in a container with relative positioning (so tool tips do not get messed up)

var margin = { top: 0, right: 0, bottom: 0, left: 0 },
width = width - margin.left - margin.right,
height = height - margin.top - margin.bottom;

var container = div.append("div").style("position", "relative");

var svg = container
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .style("font", "10px sans-serif")
          .style("overflow", "hidden");

// Treemap
// Adapted from https://observablehq.com/@d3/treemap

format = d3.format(",d")
color = d3.scaleOrdinal(d3.schemeCategory10)

treemap = data => d3.treemap()
    .size([width, height])
    .padding(1)
    .round(true)
    (d3.hierarchy(data)
    .eachBefore(function(d) { d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; })
    .sum(d => d.value)
    .sort((a, b) => b.value - a.value))

const root = treemap(data);

const leaf = svg.selectAll("g")
  .data(root.leaves())
  .join("g")
  .attr("transform", d => `translate(${d.x0},${d.y0})`);

leaf.append("title")
    .text(d => `${d.ancestors().reverse().map(d => d.data.name).join("/")}\n${format(d.value)}`);

leaf.append("rect")
      .attr("id", d=>  d.data.id)
      .attr("fill", d => { while (d.depth > 1) d = d.parent; return color(d.data.name); })
      .attr("fill-opacity", 0.6)
      .attr("width", d => d.x1 - d.x0)
      .attr("height", d => d.y1 - d.y0);

leaf.append("clipPath")
    .attr("id", d=> "clip-" + d.data.id)
    .append("use")
    .attr("xlink:href", d => "#" + d.data.id);

leaf.append("text")
    .attr("clip-path", d => "url(#clip-"+d.data.id+")")
    .selectAll("tspan")
    .data(d => d.data.name.split(/(?=[A-Z][a-z])|\s+/g).concat(format(d.value)))
    .join("tspan")
    .attr("x", 3)
    .attr("y", (d, i, nodes) => `${(i === nodes.length - 1) * 0.3 + 1.1 + i * 0.9}em`)
    .attr("fill-opacity", (d, i, nodes) => i === nodes.length - 1 ? 0.7 : null)
    .text(d => d);
