// !preview r2d3 data = read.csv("motion-charts/wealth-health.csv", stringsAsFactors = FALSE),  d3_version = 5, dependencies = list("lib/d3-delaunay.min.js", "lib/d3.v5.min.js", "motion-charts/motion-chart.css"), container = "div", options = list(labels = list(x = "GDP Per Capita (2010 US $)", y = "Life Expectancy (Years)"))

const wrapper = div.append("div")
  .attr("class", "r2d3-wrapper")
  .style("position", "relative");

var margin = {top: 19.5, right: 19.5, bottom: 19.5, left: 39.5},
    width = width - margin.right,
    height = height - margin.top - margin.bottom;

const svg = wrapper.append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");  

var tExtent = d3.extent(data.map(function(d) { return d.t;}));

var xScale, yScale, zScale, cScale;

xScale = d3.scaleLog().domain(d3.extent(data.map(function(d) {return d.x;}))).range([0, width]);
yScale = d3.scaleLinear().domain(d3.extent(data.map(function(d) {return d.y;}))).range([height, 0]);
zScale = d3.scaleSqrt().domain(d3.extent(data.map(function(d) {return d.z;}))).range([0, 40]),
cScale = d3.scaleOrdinal(d3.schemeCategory10);

var xAxis = d3.axisBottom(xScale).ticks(12, d3.format(",d")),
    yAxis = d3.axisLeft(yScale).ticks(12, d3.format(",d"));

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis);

svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", width)
    .attr("y", height - 6)
    .text(options.labels.x);

svg.append("text")
    .attr("class", "y label")
    .attr("text-anchor", "end")
    .attr("y", 6)
    .attr("dy", ".75em")
    .attr("transform", "rotate(-90)")
    .text(options.labels.y);
  
var label = svg.append("text")
    .attr("class", "year label")
    .attr("text-anchor", "end")
    .attr("y", height - 24)
    .attr("x", width)
    .text(d3.min(data.map(function(d) {return d.t;})));

var current_data = dataAt(1800);
var circle = svg.selectAll("circles")
  .data(current_data)
  .enter()
  .append("circle")
  .attr("class", "circles")
  .style("stroke", "black")
  .style("stroke-opacity", 0.25);
  
circle.call(position).sort(order);

var overlay = svg
  .append("rect")
  .attr("width", width)
  .attr("height", height)
  .attr("fill", "transparent")
  .on('mousemove', mousemoved)
  .on('mouseleave', mouseleft);

var tooltip = wrapper
  .append("div")
  .attr("class", "tooltip")
  .style("position", "absolute")
  .style('padding', '8px')
  .style('pointer-events', 'none')
  .style("background", "white")
  .style("padding", "3px")
  .style("margin", 0)
  .style("border-style", "solid")
  .style("border-width", "1px")
  .style("border-color", "black")
  .style('display', 'none');

var delaunay =  d3.Delaunay.from(current_data, d => xScale(d.x), d => yScale(d.y));
var voronoi = delaunay.voronoi([0, 0, width, height]);
const radius = width/15;

var box = label.node().getBBox();
var overlay = svg.append("rect")
      .attr("class", "overlay")
      .attr("x", box.x)
      .attr("y", box.y)
      .attr("width", box.width)
      .attr("height", box.height)
      .on("mouseover", enableInteraction);
      
var hovercircle = svg
  .append("circle")
  .attr("class", "hovercircle")
  .style("fill", "none")
  .style("stroke", "black")
  .style("opacity", 0);

svg.transition()
    .duration(30000)
    .ease(d3.easeLinear)
    .tween("year", tweenYear)
    .on("end", enableInteraction);

function mousemoved() {
  const [mx, my] = d3.mouse(this);
  hover = find(mx, my);
  if (!hover) return mouseleft();
  
   tooltip
    .style("display", "block")
    .style("top", yScale(hover.y) - zScale(hover.z) - 5   + "px")
    .style("left", xScale(hover.x) +  "px")
    .html("<div><strong>" + hover.key + "</strong></div>");
    
  hovercircle
    .attr("cx", xScale(hover.x))
    .attr("cy", yScale(hover.y))
    .attr("r", zScale(hover.z))
    .style("opacity", 1);
  
  }

function mouseleft(){
  tooltip.style('display', 'none');
  hovercircle.style("opacity", 0);
}

function find(mx, my){
   const idx = delaunay.find(mx, my);
   
   if (idx !== null) {
    const datum = current_data[idx];
    const d = distance(xScale(datum.x), yScale(datum.y), mx, my);
  
    return d < radius ? datum : null;
}

 return null;
}

distance = (px, py, mx, my) => {
  const a = px - mx;
  const b = py - my;

 return Math.sqrt(a * a + b * b);
};

function dataAt(year){
  return data.filter(function(d){return d.t == year;});
}

function enableInteraction() {
  var yearScale = d3.scaleLinear()
      .domain(d3.extent(data.map(function(d){return d.t;})))
      .range([box.x + 10, box.x + box.width - 10])
      .clamp(true);

  svg.transition().duration(0);

  overlay
      .on("mouseover", mouseover)
      .on("mouseout", mouseout)
      .on("mousemove", mousemove)
      .on("touchmove", mousemove);

  function mouseover() {
    label.classed("active", true);
  }

  function mouseout() {
    label.classed("active", false);
  }

  function mousemove() {
    displayYear(yearScale.invert(d3.mouse(this)[0]));
  }
}

function displayYear(year) {
  current_data = dataAt(Math.round(year));
  label.text(Math.round(year));
  svg.selectAll(".circles").data(current_data).call(position).sort(order);
  
  delaunay =  d3.Delaunay.from(current_data, d => xScale(d.x), d => yScale(d.y));
  voronoi = delaunay.voronoi([0, 0, width, height]);
}

function tweenYear() {
  var year = d3.interpolateNumber(tExtent[0], tExtent[1]);
  return function(t) { displayYear(year(t)); };
}


function position(){
  circle
      .transition()
      .duration(75)
      .delay(0)
      .attr("cx", d => xScale(d.x))
      .attr("cy", d => yScale(d.y))
      .attr("r", d => zScale(d.z))
      .attr("fill", d => cScale(d.c));
  
}

function order(a, b) {
  return zScale(b) - zScale(a);
}
