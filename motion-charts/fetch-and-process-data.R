library(magrittr)

pop <- readr::read_csv("motion-charts/population_total.csv") %>% 
  tidyr::gather("year", "population", -1) 

inc <- readr::read_csv("motion-charts/income_per_person_gdppercapita_ppp_inflation_adjusted (1).csv") %>%
  tidyr::gather("year", "income", -1)

le <- readr::read_csv("motion-charts/life_expectancy_years.csv") %>%
  tidyr::gather("year", "life_expectancy", -1)

country_region <- readxl::read_xlsx("motion-charts/Data Geographies - v1 - by Gapminder.xlsx", 2) %>%
  dplyr::select(country = name, region = four_regions)

df <- dplyr::left_join(pop, inc) %>% 
  dplyr::left_join(le) %>%
  dplyr::left_join(country_region) %>%
  tidyr::gather("variable", "value", -c(1:2, 6)) %>%
  dplyr::mutate(year = as.numeric(year)) %>%
  dplyr::mutate(value = as.numeric(value)) %>%
  dplyr::filter(year <= 2020)

remove_key <- df %>%
  dplyr::group_by(country, variable) %>%
  dplyr::summarise(missing_per = sum(is.na(value))/dplyr::n()) %>%
  dplyr::ungroup() %>%
  dplyr::arrange(-missing_per) %>%
  dplyr::filter(missing_per > 0.5) %>%
  dplyr::select(country) %>%
  dplyr::pull()

df <- df %>% 
  dplyr::filter(!country %in% remove_key) %>%
  tidyr::spread(variable, value )

df$region[is.na(df$region)] <- "europe"

df$region <- paste0(toupper(substr(df$region, 1,1)), substr(df$region, 2, nchar(df$region)))

df <- df %>% 
  dplyr::select(key = country, c = region, t = year, x = income, y = life_expectancy, z = population)

write.csv(df, "motion-charts/wealth-health.csv", row.names = FALSE)
