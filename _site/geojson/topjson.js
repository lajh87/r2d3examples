// !preview r2d3 data = jsonlite::read_json("geojson/us.json"), d3_version = 3, dependencies = list("lib/topojson.min.js", "geojson/cartogram.css")

var margin = {top: 19.5, right: 19.5, bottom: 19.5, left: 39.5},
    width = width - margin.right,
    height = height - margin.top - margin.bottom;

svg = svg.attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");  

var path = d3.geo.path();

 svg.append("path")
      .attr("stroke-width", 0.5)
      .datum(topojson.feature(data, data.objects.states))
      .style("fill", "none")
      .style("stroke", "black")
      .attr("d", path);