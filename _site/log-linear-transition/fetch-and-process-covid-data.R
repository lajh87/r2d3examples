library(magrittr)
covid_geo <- readr::read_csv("https://opendata.ecdc.europa.eu/covid19/casedistribution/csv")

cum_deaths_since_5th <- covid_geo %>%
  tibble::as_tibble() %>%
  dplyr::mutate(dateRep = as.Date(dateRep, "%d/%m/%Y")) %>%
  dplyr::arrange(countriesAndTerritories, dateRep) %>%
  dplyr::group_by(countriesAndTerritories) %>%
  dplyr::mutate(cumCases = cumsum(cases)) %>%
  dplyr::mutate(cumDeaths = cumsum(deaths)) %>%
  dplyr::filter(cumDeaths >= 5) %>%
  dplyr::mutate(DaysSince5thDeath = 0:(dplyr::n()-1)) %>%
  dplyr::ungroup() %>%
  dplyr::select(key = countriesAndTerritories, x = DaysSince5thDeath,  y = cumDeaths) %>%
  dplyr::mutate(key = gsub("_", "\ ", key))

write.csv(cum_deaths_since_5th, "log-linear-transition/covid-cum-deaths-since-5th.csv", row.names = FALSE)
