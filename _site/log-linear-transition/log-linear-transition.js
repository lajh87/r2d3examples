// !preview r2d3 data = read.csv("log-linear-transition/covid-cum-deaths-since-5th.csv"),  d3_version = 5, dependencies = list("lib/d3-delaunay.min.js", "lib/d3.v5.min.js"), container = "div"

// Define Widths
var margin = { top: 40, right: 0, bottom: 30, left: 50 },
width = width - margin.left - margin.right,
height = height - margin.top - margin.bottom;

// Process Data
var nested = d3.nest()
               .key(function(d) { return d.key; })
               .entries(data);
               
var keys = nested.map(function(d){ return d.key;});

var formatNumber = d3.format(",d");

var container = div.append("div").style("position", "relative");

var svg = container
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom);

var chart = svg
     .append("g")
     .attr("id","chart")
     .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Axis
var xScale = d3.scaleLinear().range([0, width]);
xScale.domain([0, d3.max(data, function(d) {return d.x;})]);

var xAxis = d3.axisBottom().scale(xScale);
var yScale = d3.scaleLinear().range([height, 5]);

yScale.domain([5, d3.max(data, function(d) {return d.y;})]);
var yAxis = d3.axisLeft().scale(yScale).ticks(10,".0s");

var loglinear = chart
  .append("text")
  .attr("y", 0)
  .attr("dy", "-0.25em")
  .attr("x", -margin.left)
  .attr("dx", "+0.5em")
  .text("Linear")
  .style("cursor", "pointer")
  .on('click', switchScale);
  
chart
  .append("text")
  .attr("y", height+margin.bottom)
  .attr("dy", "-0.75em")
  .attr("x", width)
  .style("text-anchor", "end")
  .style("font-size", 10)
  .text("Days");

chart
  .append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 0-margin.left)
  .attr("x", 0-margin.top)
  .attr("dy", "0.75em")
  .style("font-size", 10)
  .style("text-anchor", "start")
  .text("Deaths");
  
chart
    .append("g")
    .attr("class", "yAxis")
    .call(yAxis);
  
chart
 .append("g")
 .attr("class", "x axis")
 .attr("transform", "translate(0," + height + ")")
 .call(xAxis);
 
// Lines
var line = d3.line()
    .x(function(d) { return xScale(d.x); }) 
    .y(function(d) { return yScale(d.y); })
    .curve(d3.curveMonotoneX);
    
var lines = chart
   .selectAll(".lines")
   .data(nested)
   .enter()
   .append("g")
   .attr("class", function(d) {return d.key;})
   .append("path")
   .attr("class", "line")
   .style("fill", "none")
   .attr("d", function(d) {return line(d.values);})
   .style("stroke", "#d2d2d2");
   
var hoverline = chart
  .append("path")
  .attr("class", "hoverline")
  .style("fill", "none")
  .style("stroke", "black");
  
var hovercircle = chart
  .append("circle")
  .attr("class", "hovercircle")
  .style("fill", "none")
  .style("stroke", "black")
  .style("opacity", 0)
  .attr("r", 4);
  
// Tooltip
chart
  .append("rect")
  .attr("width", width)
  .attr("height", height)
  .attr("fill", "transparent")
  .on('mousemove', mousemoved)
  .on('mouseleave', mouseleft);

var delaunay =  d3.Delaunay.from(data, d => xScale(d.x), d => yScale(d.y));
var voronoi = delaunay.voronoi([0, 0, width, height]);
const radius = width/15;

var tooltip = container
.append("div")
.attr("class", "tooltip")
.style("position", "absolute")
.style('padding', '8px')
.style('pointer-events', 'none')
.style('display', 'none');
  
function mousemoved() {
  const [mx, my] = d3.mouse(this);
  hover = find(mx, my);
  if (!hover) return mouseleft();
  
   tooltip
    .style("display", "block")
    .style("top", yScale(hover.y)-10  + "px")
    .style("left", xScale(hover.x) + "px")
    .html("<div><strong>" + hover.key + "</strong><br>"+formatNumber(hover.y)+"</br></div>");
    
  var selectedline = chart.select("."+hover.key.split(' ').join(".")).select(".line").data();
  hoverline.attr("d", function(d) {
    return line(selectedline[0].values);
    
  });
  
  hovercircle
    .attr("cx", xScale(hover.x))
    .attr("cy", yScale(hover.y))
    .style("opacity", 0.5);
  }

function find(mx, my){
  const idx = delaunay.find(mx, my);
 
   if (idx !== null) {
    const datum = data[idx];
    const d = distance(xScale(datum.x), yScale(datum.y), mx, my);
  
    return d < radius ? datum : null;
}

 return null;
}

distance = (px, py, mx, my) => {
  const a = px - mx;
  const b = py - my;

 return Math.sqrt(a * a + b * b);
};

function mouseleft(){
  tooltip.style('display', 'none');
  hoverline.attr("d", function(d) {return line("");});
  hovercircle.style("opacity",0);
}

// Transition Between Log and Linear
function switchScale(){
  // Axis
  if(loglinear.text() == "Linear"){
    loglinear.transition().delay(500).text("Log");
    yScale = d3.scaleLog().range([height, 0]);
    yScale.domain([5, d3.max(data, function(d) {return d.y;})]);
    yAxis = d3.axisLeft().scale(yScale)
       .ticks(10, d3.format(".1s"));
     
 } else{
    loglinear.transition().delay(500).text("Linear");
    yScale = d3.scaleLinear().range([height, 0]);
     yScale.domain([5, d3.max(data, function(d) {return d.y;})]);
     yAxis = d3.axisLeft().scale(yScale)
       .ticks(10, d3.format(".1s"));
 }
 
 chart
   .selectAll(".yAxis")
   .transition().delay(500).duration(1000)
   .call(yAxis.scale(yScale));
  
  // Lines
  chart
    .selectAll(".line")
    .transition().delay(500).duration(1000)
    .attr("d", function(d){
      return line(d.values);
    });
  
  // Voronoi
  // Set Up Voronoi Points
 delaunay =  d3.Delaunay.from(data, d => xScale(d.x), d => yScale(d.y));
 voronoi = delaunay.voronoi([0, 0, width, height]);
  hoverline.attr("d", function(d) {return line("");});
 
}

switchScale();




